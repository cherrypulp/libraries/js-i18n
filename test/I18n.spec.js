import assert from 'assert';
import chai from 'chai';
import I18n from '../src/I18n';

const expect = chai.expect;
const should = chai.should;

describe('I18n.js', () => {
    beforeEach(() => {
        global.window = {};
        global.document = {
            createElement(name) {
                const obj = {
                    name,
                    content: null,
                };
                return Object.defineProperties(obj, {
                    innerHTML: {
                        set(value) {
                            obj.content = value;
                        },
                    },
                    value:     {
                        get() {
                            return obj.content;
                        },
                    },
                });
            },
        };
    });

    describe('#constructor()', () => {
        it('should instantiate with given parameters', () => {
            const translations = {
                foo: 'BAR',
            };
            const locale = 'en';
            const i18n = new I18n(translations, locale);

            expect(i18n.locale).to.equal(locale);
            expect(i18n.get('foo', locale)).to.equal(translations.foo);
        });

        it('should instantiate without parameters', () => {
            const locale = 'en';
            const i18n = new I18n();

            expect(i18n.locale).to.equal(locale);
            expect(i18n.get('foo')).to.equal('foo');
        });

        it('should create a global called "translations" in window', () => {
            const i18n = new I18n();

            expect(window.translations).to.not.be.undefined;
        });

        it('should retrieve translations from the global called "translations" in window', () => {
            window.translations = {
                foo: 'BAR',
            };
            const i18n = new I18n();

            expect(window.translations).to.not.be.undefined;
            expect(i18n.get('foo')).to.equal(window.translations.foo);
        });
    });

    describe('#add()', () => {
        it('should add given translations', () => {
            const translations = {
                foo: 'BAR',
            };
            const i18n = new I18n({});

            i18n.add(translations);

            expect(i18n.get('foo')).to.equal(translations.foo);
        });

        it('should add given translations for the given language', () => {
            const locale = 'es';
            const translations = {
                foo: 'BAR',
            };
            const i18n = new I18n({});

            i18n.add(translations, locale);

            expect(i18n.get('foo', locale)).to.equal(translations.foo);
        });
    });

    describe('#choice()', () => {
        it('should return given key if the translation is not found', () => {
            const translations = {};
            const i18n = new I18n(translations, 'en', {
                forceDisplayKeys: true,
            });

            expect(i18n.choice('foo', 1)).to.equal('foo');
        });

        it('should return the key if the translation is not found', () => {
            const translations = {};
            const i18n = new I18n(translations);

            expect(i18n.choice('foo', 1)).to.be.equal('foo');
        });

        it('should return singular translation', () => {
            const translations = {
                foo: 'singular|plural',
            };
            const i18n = new I18n(translations);

            expect(i18n.choice('foo', 1)).to.equal(translations.foo.split('|')[0]);
        });

        it('should return plural translation', () => {
            const translations = {
                foo: 'singular|plural',
            };
            const i18n = new I18n(translations);

            expect(i18n.choice('foo', 2)).to.equal(translations.foo.split('|')[1]);
        });

        it('should return translation that match count', () => {
            const translations = {
                foo: '{0}fez|[3,5]bar|[9]baz|[10,*]fiz',
            };
            const i18n = new I18n(translations);

            expect(i18n.choice('foo', 0)).to.equal('fez');
            expect(i18n.choice('foo', 2)).to.equal('foo');
            expect(i18n.choice('foo', 4)).to.equal('bar');
            expect(i18n.choice('foo', 9)).to.equal('baz');
            expect(i18n.choice('foo', 92)).to.equal('fiz');
            expect(i18n.choice('foo', -2)).to.equal('foo');
        });
    });

    describe('#fetch()', () => {
        it('should return the given key content', () => {
            const locale = 'en';
            const translations = {
                foo: 'BAR',
            };
            const i18n = new I18n(translations);

            expect(i18n.fetch(`${locale}.foo`)).to.equal(translations.foo);
        });

        it('should return undefined if the given key not exists', () => {
            const locale = 'en';
            const translations = {};
            const i18n = new I18n(translations);

            expect(i18n.fetch(`${locale}.foo`)).to.be.undefined;
        });
    });

    describe('#get()', () => {
        it('should return the given key content', () => {
            const translations = {
                foo: 'BAR',
            };
            const i18n = new I18n(translations);

            expect(i18n.get('foo')).to.equal(translations.foo);
        });

        it('should return the given key with dot content', () => {
            const translations = {
                foo: {
                    bar: 'BAZ',
                },
            };
            const i18n = new I18n(translations);

            expect(i18n.get('foo.bar')).to.equal(translations.foo.bar);
        });

        it('should return the key if the given key not exists', () => {
            const translations = {};
            const i18n = new I18n(translations);

            expect(i18n.get('foo')).to.equal('foo');
            expect(i18n.get('foo.bar')).to.equal('foo.bar');
            expect(i18n.get('foo', null, 'fr')).to.equal('foo');
        });

        it('should return the given key content with replaced data', () => {
            const translations = {
                foo: 'BAR :baz',
            };
            const data = {
                baz: 'bat',
            };
            const i18n = new I18n(translations);

            expect(i18n.get('foo', data)).to.equal('BAR bat');
        });

        it('should return the given key content with replaced data capitalized', () => {
            const translations = {
                foo: 'BAR :Baz',
            };
            const data = {
                baz: 'bat',
            };
            const i18n = new I18n(translations);

            expect(i18n.get('foo', data)).to.equal('BAR Bat');
        });

        it('should return the given key content with replaced data uppercase', () => {
            const translations = {
                foo: 'BAR :BAZ',
            };
            const data = {
                baz: 'bat',
            };
            const i18n = new I18n(translations);

            expect(i18n.get('foo', data)).to.equal('BAR BAT');
        });

        it('should return the given key content in specified locale', () => {
            const translations = {
                foo: 'BAR',
            };
            const i18n = new I18n();
            i18n.setLocale('fr');
            i18n.set(translations);
            i18n.setLocale('en');

            expect(i18n.get('foo', null, 'fr')).to.equal(translations.foo);
        });

        it('should store not found keys in global _notFounds', () => {
            const translations = {};
            const i18n = new I18n(translations);

            i18n.get('foo');
            i18n.get('foo.bar');
            i18n.get('baz');

            expect(window.translations._notFounds).to.have.members(['foo', 'foo.bar', 'baz']);
        });
    });

    describe('#has()', () => {
        it('should return true if the given key exists', () => {
            const translations = {
                foo: 'BAR',
            };
            const i18n = new I18n(translations);

            expect(i18n.has('foo')).to.be.true;
        });

        it('should return true if the given key exists in given locale', () => {
            const locale = 'fr';
            const translations = {
                foo: 'BAR',
            };
            const i18n = new I18n();
            i18n.set(translations, locale);

            expect(i18n.has('foo', locale)).to.be.true;
        });

        it('should return false if the given key not exists', () => {
            const translations = {};
            const i18n = new I18n(translations);

            expect(i18n.has('foo')).to.be.false;
        });

        it('should return false if the given key not exists in given locale', () => {
            const locale = 'fr';
            const translations = {
                foo: 'BAR',
            };
            const i18n = new I18n();
            i18n.set(translations, locale);

            expect(i18n.has('foo', 'en')).to.be.false;
        });
    });

    describe('#setLocale()', () => {
        it('should set the given locale as the current one', () => {
            const locale = 'en';
            const i18n = new I18n();
            i18n.setLocale(locale);

            expect(i18n.locale).to.equal(locale);
        });
    });

    describe('#set()', () => {
        it('should set the given translations', () => {
            const translations = {
                foo: 'BAR',
            };
            const i18n = new I18n();
            i18n.set(translations);

            expect(i18n.has('foo')).to.be.true;
        });

        it('should set the given translations in the given locale', () => {
            const locale = 'fr';
            const translations = {
                foo: 'BAR',
            };
            const i18n = new I18n();
            i18n.set(translations, locale);

            expect(i18n.has('foo', locale)).to.be.true;
        });
    });

    describe('static#matchChoiceCount()', () => {
        it('should match translation with given count', () => {
            const translations = {
                foo: '{0}BAR',
                bar: '[3-5]BAZ',
            };

            expect(I18n.matchChoiceCount(translations.foo, 0)).to.equal('BAR');
            expect(I18n.matchChoiceCount(translations.bar, 3)).to.equal('BAZ');
        });
    });

    describe('static#replaceString()', () => {
        it('should set the given translations', () => {
            const translations = {
                foo: 'bar :BAZ',
            };

            expect(I18n.replaceString(translations.foo, {baz: 'bat'})).to.equal('bar BAT');
        });
    });
});
