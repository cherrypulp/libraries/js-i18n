import assert from 'assert';
import chai from 'chai';
import I18n, {createI18n, useI18n, __, choice} from '../src/index';

const expect = chai.expect;
const should = chai.should;

describe('I18n/composition.js', () => {
    beforeEach(() => {
        global.window = {};
        global.document = {
            createElement(name) {
                const obj = {
                    name,
                    content: null,
                };
                return Object.defineProperties(obj, {
                    innerHTML: {
                        set(value) {
                            obj.content = value;
                        },
                    },
                    value:     {
                        get() {
                            return obj.content;
                        },
                    },
                });
            },
        };
    });

    describe('createI18n()', () => {
        it('should return a new instance of I18n', () => {
            const settings = {
                defaultLocale: 'en',
                translations: {
                    foo: 'BAR',
                },
            };
            const i18n = createI18n(settings.translations, settings.defaultLocale);

            expect(i18n).not.to.equal(createI18n(settings.translations, 'fr'));
            expect(i18n.locale).to.equal(settings.defaultLocale);
            expect(i18n.get('foo', settings.defaultLocale)).to.equal(settings.translations.foo);
        });
    });

    describe('useI18n()', () => {
        it('should return a global instance of I18n', () => {
            const settings = {
                defaultLocale: 'en',
                translations: {
                    foo: 'BAR',
                },
            };
            const i18n = useI18n(settings.translations, settings.defaultLocale);

            expect(i18n).to.equal(useI18n());
            expect(i18n.locale).to.equal(settings.defaultLocale);
            expect(i18n.get('foo', settings.defaultLocale)).to.equal(settings.translations.foo);
        });
    });
});
