/**
 * Simple I18n wrapper around a global object.
 * @example
 * import I18n from '@cherrypulp/i18n';
 *
 * const i18n = new I18n({
 *     foo: 'bar',
 * }, 'en');
 *
 * i18n.add({'baz': 'Hello :word!'});
 *
 * alert(i18n.get('foo')); // return 'bar'
 * alert(i18n.get('foo', {word: 'wold'}); // return 'Hello world!'
 */
export default class I18n {
    /**
     * Get a specific label. This method is an alias of 'get()'.
     * @example
     * // @example {foo: 'bar'}
     * i18n.__('foo', 'en'); // return "bar"
     * @example
     * // @example {hello: 'Hello :name'}
     * i18n.__('hello', {name: 'world'}, 'en'); // return "Hello world"
     * @example
     * // @example {hello: 'Hello :Name'}
     * i18n.__('hello', {name: 'world'}, 'en'); // return "Hello World"
     * @example
     * // @example {hello: 'Hello :NAME'}
     * i18n.__('hello', {name: 'world'}, 'en'); // return "Hello WORLD"
     * @param key
     * @param {Object} data (default: null)
     * @param {String} locale (default: this.locale)
     * @return {String}
     * @alias get
     */
    __(key, data = null, locale = null) {
        return this.get(key, data, locale);
    }

    /**
     * Add labels to a given language
     * @example
     * i18n.add({
     *     'foo': 'bar',
     * }, 'en');
     * @param {Object} translations
     * @param {String} locale (default: this.locale)
     */
    add(translations, locale = null) {
        if (!locale) {
            locale = this.locale;
        }

        this.translations.set(locale, Object.assign({}, this.translations.get(locale), translations));
    }

    /**
     * Apply a condition on count of key
     *
     * @param {String} key
     * @param {Number} count (default: 1)
     * @param {Object} data (default: null)
     * @param {String} locale (default: this.locale)
     * @returns {String}
     */
    choice(key, count = 1, data = null, locale = null) {
        if (!locale) {
            locale = this.locale;
        }

        let translation = null;
        const translations = this.fetch(`${locale}.${key}`);

        if (!translations) {
            if (this.options.storeNotFounds) {
                window[this.options.globalName]._notFounds.push(key);
            }

            if (this.forceDisplayKeys) {
                if (data) {
                    return this.constructor.replaceString(key, data);
                }

                return key;
            }

            return '';
        }

        const parts = translations.split('|');

        parts.some((p) => {
            translation = this.constructor.matchChoiceCount(p, count);
            return translation;
        });

        // not a choice translation, check between singular/plural
        if (translation === false) {
            translation = count > 1 ? parts[1] : parts[0];
        }

        return this._returnString(key, translation, data);
    }

    /**
     * @example
     * const i18n = new I18n({
     *     'foo': 'bar',
     * });
     *
     * @param {Object} translations
     * @param {String} defaultLocale (default: 'en')
     * @param {Object} options (default: {globalName: 'translations', forceDisplayKeys: false})
     */
    constructor(translations = {}, defaultLocale = 'en', options = {}) {
        options = Object.assign({
            globalName:       'translations',
            forceDisplayKeys: true,
            storeNotFounds:   true,
        }, options);

        this.forceDisplayKeys = options.forceDisplayKeys;
        this.locale = defaultLocale;
        this.translations = new Map();

        if (window === undefined) {
            options.globalName = false;
            options.storeNotFounds = false;
        }

        if (options.globalName) {
            if (window[options.globalName] === undefined) {
                window[options.globalName] = {};
            }

            translations = Object.assign({}, window[options.globalName], translations);

            if (options.storeNotFounds) {
                window[options.globalName]._notFounds = [];
            }
        }

        this.set(translations, this.locale);

        this.options = options;
    }

    // @TODO - see http://i18njs.com/#contexts
    // context(key, context = null, data = null, locale = null) {
    //     if (!locale) {
    //         locale = this.locale;
    //     }
    //
    //     // "[{gender: female}]
    // }

    /**
     * Decode HTML Entities
     * @param {String} source
     * @return {String}
     */
    static decodeHtml(source) {
        if (typeof document !== 'undefined') {
            const txt = document.createElement('textarea');
            txt.innerHTML = source;
            return txt.value;
        }

        return source;
    }

    /**
     * Retrieve a translation.
     * @example
     * i18n.fetch('en.foo'); // return "bar"
     * @param {String} key
     * @return {String}
     */
    fetch(key) {
        const keys = key.split('.');
        const locale = keys.shift();

        let source = this.translations.get(locale);

        keys.forEach((k) => {
            if (source) {
                source = source[k];
            }
        });

        return source;
    }

    /**
     * Get a specific label
     * @example
     * // @example {foo: 'bar'}
     * i18n.get('foo', 'en'); // return "bar"
     * @example
     * // @example {hello: 'Hello :name'}
     * i18n.get('hello', {name: 'world'}, 'en'); // return "Hello world"
     * @example
     * // @example {hello: 'Hello :Name'}
     * i18n.get('hello', {name: 'world'}, 'en'); // return "Hello World"
     * @example
     * // @example {hello: 'Hello :NAME'}
     * i18n.get('hello', {name: 'world'}, 'en'); // return "Hello WORLD"
     * @param {String} key
     * @param {Object} data (default: null)
     * @param {String} locale (default: this.locale)
     * @return {String}
     */
    get(key, data = null, locale = null) {
        if (typeof data === 'string') {
            locale = data;
            data = null;
        }

        if (!locale) {
            locale = this.locale;
        }

        const content = this.fetch(`${locale}.${key}`);
        return this._returnString(key, content, data);
    }

    /**
     * Checking if a translation key exists.
     * @param key
     * @param {String} locale (default: this.locale)
     * @return {boolean}
     */
    has(key, locale = null) {
        if (!locale) {
            locale = this.locale;
        }

        return this.fetch(`${locale}.${key}`) !== undefined;
    }

    /**
     * Match the translation limit with the count.
     * @param {String} translation
     * @param {Number} count
     * @return {null|*}
     */
    static matchChoiceCount(translation, count) {
        const match = translation.match(/^[{[]([^[\]{}]*)[}\]](.*)/);

        if (!match) {
            return false;
        }

        if (match[1].includes(',')) {
            const [from, to] = match[1].split(',', 2);

            if (
                (to === '*' && count >= from)
                || (from === '*' && count <= to)
                || (count >= from && count <= to)
            ) {
                // eslint-disable-next-line consistent-return
                return match[2];
            }
        }

        // eslint-disable-next-line consistent-return
        return parseInt(match[1], 10) === count ? match[2] : null;
    }

    /**
     * Apply data object values to given template.
     * @example
     * I18n.replaceString('Hello :name', {name: 'world'}); // return "Hello world"
     * @example
     * I18n.replaceString('Hello :Name', {name: 'world'}); // return "Hello World"
     * @example
     * I18n.replaceString('Hello :NAME', {name: 'world'}); // return "Hello WORLD"
     * @param {String} translation
     * @param {Object} data
     * @return {String}
     */
    static replaceString(translation, data) {
        if (!data) {
            return translation;
        }

        return Object.entries(data)
            .reduce((acc, [key, value]) => {
                value = String(value);
                const placeholder = key.toLowerCase();
                return acc
                    .replace(`:${placeholder}`, value)
                    .replace(`:${placeholder.toUpperCase()}`, value.toUpperCase())
                    .replace(`:${placeholder.charAt(0).toUpperCase()}${placeholder.slice(1)}`, `${value.charAt(0).toUpperCase()}${value.slice(1)}`);
            }, translation);
    }

    /**
     * Return formatted content or key based on setup.
     * @param key
     * @param content
     * @param data
     * @return {String|string|*}
     * @private
     */
    _returnString(key, content, data) {
        if (typeof content !== 'string' && this.forceDisplayKeys) {
            content = key;

            if (this.options.storeNotFounds) {
                window[this.options.globalName]._notFounds.push(key);
            }
        }

        if (data) {
            content = this.constructor.replaceString(content, data);
        }

        return this.constructor.decodeHtml(content);
    }

    /**
     * Set current language.
     * @param {String} locale
     */
    setLocale(locale) {
        this.locale = locale;
    }

    /**
     * Set translations in current language
     * @example
     * i18n.set({
     *     foo: 'bar',
     * });
     * @param {Object} translations
     * @param {String} locale (default: this.locale)
     */
    set(translations, locale = null) {
        if (!locale) {
            locale = this.locale;
        }

        this.translations.set(locale, translations);
    }
}
