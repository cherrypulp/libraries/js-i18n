import I18n from './I18n';

let globalInstance = null;

/**
 * Create a new I18n instance and return it.
 * @param translations
 * @param defaultLocale
 * @param options
 * @return {I18n}
 */
export function createI18n(translations = {}, defaultLocale = 'en', options = {}) {
    return new I18n(translations, defaultLocale, options);
}

/**
 * Retrieve or create a globalInstance.
 * @param translations
 * @param defaultLocale
 * @param options
 * @return {I18n}
 */
export function useI18n(translations = {}, defaultLocale = 'en', options = {}) {
    if (!globalInstance) {
        globalInstance = createI18n(translations, defaultLocale, options);
    }

    return globalInstance;
}

/**
 * Retrieve a label.
 * @param key
 * @param data
 * @param language
 * @return {String}
 */
export function __(key, data = {}, language = null) {
    return useI18n().get(key, data, language);
};

/**
 * @param key
 * @param count
 * @param data
 * @param language
 * @return {String}
 */
export function choice(key, count = 1, data = {}, language = null) {
    return useI18n().choice(key, count, data, language);
};
