import I18n from './I18n';
import {createI18n, useI18n, __, choice} from './composition';
import VueI18n from './vue-i18n';

export {
    createI18n,
    useI18n,
    __,
    choice,
    VueI18n,
};

export default I18n;
