import I18n from './I18n';

/**
 * Vue I18n Plugin
 * @example
 * const options = {
 *     labels: {
 *         foo: 'bar',
 *     },
 * };
 *
 * Vue.use(VueI18n, options);
 *
 * // then
 *
 * // - in JS
 * this.__('foo'); // return 'bar';
 *
 * // - in Template
 * {{ __('foo') }}
 */
export default {
    name: 'vue-i18n',
    production: process.env.NODE_ENV === 'production',
    install(Vue, settings = {}) {
        settings = Object.assign({
            instance: null,
            translations: {},
            language: 'en',
            options: {},
        }, settings);

        let instance = settings.instance;

        if (!instance) {
            instance = new I18n({}, settings.language, settings.options);
        }

        instance.add(settings.translations, settings.language);

        if (typeof Vue.config !== 'undefined' && typeof Vue.config.globalProperties !== 'undefined') {
            Vue.config.globalProperties.$i18n = instance;
            Vue.config.globalProperties.__ = instance.__;
            Vue.config.globalProperties.choice = instance.choice;
        } else {
            Vue.prototype.$i18n = instance;
            Vue.$i18n = instance;
            Vue.mixin({
                methods: {
                    __: (key, data = null, locale = null) => instance.__(key, data, locale),
                    choice: (key, count = 1, data = null, locale = null) => instance.choice(key, count, data, locale),
                },
            });
        }
    },
};
