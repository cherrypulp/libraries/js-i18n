/**
 * Simple I18n wrapper around a global object.
 * @example
 * import I18n from '@cherrypulp/i18n';
 *
 * const i18n = new I18n({
 *     foo: 'bar',
 * }, 'en');
 *
 * i18n.add({'baz': 'Hello :word!'});
 *
 * alert(i18n.get('foo')); // return 'bar'
 * alert(i18n.get('foo', {word: 'wold'}); // return 'Hello world!'
 */
export default class I18n {
    /**
     * Decode HTML Entities
     * @param {String} source
     * @return {String}
     */
    static decodeHtml(source: string): string;
    /**
     * Match the translation limit with the count.
     * @param {String} translation
     * @param {Number} count
     * @return {null|*}
     */
    static matchChoiceCount(translation: string, count: number): null | any;
    /**
     * Apply data object values to given template.
     * @example
     * I18n.replaceString('Hello :name', {name: 'world'}); // return "Hello world"
     * @example
     * I18n.replaceString('Hello :Name', {name: 'world'}); // return "Hello World"
     * @example
     * I18n.replaceString('Hello :NAME', {name: 'world'}); // return "Hello WORLD"
     * @param {String} translation
     * @param {Object} data
     * @return {String}
     */
    static replaceString(translation: string, data: any): string;
    /**
     * @example
     * const i18n = new I18n({
     *     'foo': 'bar',
     * });
     *
     * @param {Object} translations
     * @param {String} defaultLocale (default: 'en')
     * @param {Object} options (default: {globalName: 'translations', forceDisplayKeys: false})
     */
    constructor(translations?: any, defaultLocale?: string, options?: any);
    /**
     * Get a specific label. This method is an alias of 'get()'.
     * @example
     * // @example {foo: 'bar'}
     * i18n.__('foo', 'en'); // return "bar"
     * @example
     * // @example {hello: 'Hello :name'}
     * i18n.__('hello', {name: 'world'}, 'en'); // return "Hello world"
     * @example
     * // @example {hello: 'Hello :Name'}
     * i18n.__('hello', {name: 'world'}, 'en'); // return "Hello World"
     * @example
     * // @example {hello: 'Hello :NAME'}
     * i18n.__('hello', {name: 'world'}, 'en'); // return "Hello WORLD"
     * @param key
     * @param {Object} data (default: null)
     * @param {String} locale (default: this.locale)
     * @return {String}
     * @alias get
     */
    __(key: any, data?: any, locale?: string): string;
    /**
     * Add labels to a given language
     * @example
     * i18n.add({
     *     'foo': 'bar',
     * }, 'en');
     * @param {Object} translations
     * @param {String} locale (default: this.locale)
     */
    add(translations: any, locale?: string): void;
    /**
     * Apply a condition on count of key
     *
     * @param {String} key
     * @param {Number} count (default: 1)
     * @param {Object} data (default: null)
     * @param {String} locale (default: this.locale)
     * @returns {String}
     */
    choice(key: string, count?: number, data?: any, locale?: string): string;
    forceDisplayKeys: any;
    locale: string;
    translations: any;
    options: any;
    /**
     * Retrieve a translation.
     * @example
     * i18n.fetch('en.foo'); // return "bar"
     * @param {String} key
     * @return {String}
     */
    fetch(key: string): string;
    /**
     * Get a specific label
     * @example
     * // @example {foo: 'bar'}
     * i18n.get('foo', 'en'); // return "bar"
     * @example
     * // @example {hello: 'Hello :name'}
     * i18n.get('hello', {name: 'world'}, 'en'); // return "Hello world"
     * @example
     * // @example {hello: 'Hello :Name'}
     * i18n.get('hello', {name: 'world'}, 'en'); // return "Hello World"
     * @example
     * // @example {hello: 'Hello :NAME'}
     * i18n.get('hello', {name: 'world'}, 'en'); // return "Hello WORLD"
     * @param {String} key
     * @param {Object} data (default: null)
     * @param {String} locale (default: this.locale)
     * @return {String}
     */
    get(key: string, data?: any, locale?: string): string;
    /**
     * Checking if a translation key exists.
     * @param key
     * @param {String} locale (default: this.locale)
     * @return {boolean}
     */
    has(key: any, locale?: string): boolean;
    /**
     * Return formatted content or key based on setup.
     * @param key
     * @param content
     * @param data
     * @return {String|string|*}
     * @private
     */
    private _returnString;
    /**
     * Set current language.
     * @param {String} locale
     */
    setLocale(locale: string): void;
    /**
     * Set translations in current language
     * @example
     * i18n.set({
     *     foo: 'bar',
     * });
     * @param {Object} translations
     * @param {String} locale (default: this.locale)
     */
    set(translations: any, locale?: string): void;
}
