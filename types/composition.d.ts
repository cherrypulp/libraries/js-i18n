/**
 * Create a new I18n instance and return it.
 * @param translations
 * @param defaultLocale
 * @param options
 * @return {I18n}
 */
export function createI18n(translations?: {}, defaultLocale?: string, options?: {}): I18n;
/**
 * Retrieve or create a globalInstance.
 * @param translations
 * @param defaultLocale
 * @param options
 * @return {I18n}
 */
export function useI18n(translations?: {}, defaultLocale?: string, options?: {}): I18n;
/**
 * Retrieve a label.
 * @param key
 * @param data
 * @param language
 * @return {String}
 */
export function __(key: any, data?: {}, language?: any): string;
/**
 * @param key
 * @param count
 * @param data
 * @param language
 * @return {String}
 */
export function choice(key: any, count?: number, data?: {}, language?: any): string;
import I18n from "./I18n";
