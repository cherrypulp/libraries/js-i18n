export default I18n;
import { createI18n } from "./composition";
import { useI18n } from "./composition";
import { __ } from "./composition";
import { choice } from "./composition";
import VueI18n from "./vue-i18n";
import I18n from "./I18n";
export { createI18n, useI18n, __, choice, VueI18n };
