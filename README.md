# I18n 

[![codebeat badge](https://codebeat.co/badges/fe5b8fc4-34f1-44a6-bcb5-33991d0517bb)](https://codebeat.co/projects/gitlab-com-cherrypulp-libraries-js-i18n-master)
[![Issues](https://img.shields.io/badge/issues-0-brightgreen)](https://gitlab.com/cherrypulp/libraries/js-i18n/issues)
[![License](https://img.shields.io/npm/l/@cherrypulp/i18n)](https://gitlab.com/cherrypulp/libraries/js-i18n/blob/master/LICENSE)
[![npm version](https://badge.fury.io/js/%40cherrypulp%2Fi18n.svg)](https://badge.fury.io/js/%40cherrypulp%2Fi18n)

Simple I18n wrapper around a global object.


## Installation

`npm install @cherrypulp/i18n`


## Quick start

### Initialization

```javascript
import I18n from '@cherrypulp/i18n';

const options = {
    globalName: 'translations', // name of the autoloaded global variable
    forceDisplayKeys: true, // display the key if the label is not found (else it return an empty string)
    storeNotFounds: true, // store every key that are not found in a variable called "_notFounds" inside the global
};

const translations = {
    foo: 'Bar',
    hello: 'Hello :name',
    user: {
        firstname: 'First name',
    },
};
const i18n = new I18n(translations, 'en', options);

// or

window.translations = {
    foo: 'bar',
    hello: 'Hello :Name',
    user: {
        firstname: 'John Doe',
    },
};
const i18n = new I18n();
```

`window.translations` is loaded by default, `translations` will be merged with it.


### Methods

#### `set`

Set translations for the given language (default language is the current one).

```javascript
i18n.set({
	bar: 'BAZ',
}, 'en');
```

#### `add`

Add translations to the given language (default language is the current one).

```javascript
i18n.add({
	foo: 'BAR',
}, 'en');
```


#### `setLocale`

Set current locale.

```javascript
i18n.setLocale('fr'); // i18n.locale return 'fr'
```

#### `has`

Return `true` if the given key exist in translations.

```javascript
i18n.has('foo'); // return true
i18n.has('bat'); // return false
```

#### `get`

> You can now use `__` as an alias of `get`

Return the translation for the given key. You can pass an object as 2nd arguments to replace placeholders in the translation.

```javascript
i18n.get('foo'); // return "bar"
i18n.get('user.firstname'); // return "John Doe"
i18n.get('bat'); // return "bat"
i18n.get('foo', null, 'fr'); // return "foo"
i18n.__('foo', null, 'fr'); // return "foo" (alias)
```

All placeholders are prefixed with a `:`.

```javascript
i18n.get('hello', {name: 'world'}); // return "Hello World"
```

If your placeholder contains all capital letters, or only has its first letter capitalized, the translated value will be capitalized accordingly:

```javascript
// @example {hello: 'Hello :NAME'}
i18n.get('hello', {name: 'john'}); // return "Hello JOHN"

// @example {hello: 'Hello :Name'}
i18n.get('hello', {name: 'john'}); // return "Hello John"
```

#### `fetch`

Retrieve a translation from the locale.

```javascript
i18n.fetch('en.foo'); // return "bar"
```

#### `choice`

Singular and plural forms are separated by a `|` (= pipe).

```javascript
// @example {apples: 'There is one apple|There are many apples'}
i18n.choice('apples', 4); // return "There are many apples"
```

You may even create more complex pluralization rules which specify translation strings for multiple number ranges:

```javascript
// @example {apples: '{0} There are none|[1,19] There are some|[20,*] There are many'}
i18n.choice('apples', 4); // return "There are some"
```



### VueJS

```javascript
import { VueI18n } from '@cherrypulp/i18n';

const settings = {
    translations: {
        foo: 'BAR',
    },
    language: 'en',
    options: {
        forceDisplayKeys: false,
    },
};

Vue.use(VueI18n, settings);

// then

this.$i18n.get('foo'); // return "BAR"
Vue.$i18n.get('foo'); // return "BAR"
this.__('foo'); // return "BAR"

// or in templates

<div>{{ $i18n.get('foo') }}</div>
<div>{{ __('foo') }}</div>
```

### Composition

```javascript
import { createApp } from 'vue';
import { createI18n, useI18n, __, choice } from '@cherrypulp/i18n';

const app = createApp();
const options = {
    translations: {
        foo: 'BAR',
    },
    language: 'en',
};

// retrieve a global instance or create one (this instance is a singleton)
const i18n = useI18n(options.translations, options.language);

// create a new instance
const i18nOtherInstance = createI18n(options.translations, options.language);

app.config.globalProperties.__ = __;

// then

i18n.get('foo'); // return "BAR"
i18n.__('foo'); // return "BAR" (alias)
__('foo'); // return "BAR" (shortcut using global instance)

// or in templates

<div>{i18n.get('foo')}</div>
<div>{__('foo')}</div>
```

## Versioning

Versioned using [SemVer](http://semver.org/).

## Contribution

Please raise an issue if you find any. Pull requests are welcome!

## Author

  + **Stéphan Zych** - [monkeymonk](https://gitlab.com/monkey_monk)

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/cherrypulp/libraries/js-i18n/blob/master/LICENSE) file for details.
